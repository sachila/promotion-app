angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel'])

	   
   .controller('AppCtrl', function ($scope, $mdDialog, $mdToast) {
   
		
		//This template opens when the bank user clicks the Confirm Transaction Button
		var acceptContentTemplate = ' \
      <md-dialog><form> \
        <md-content style="padding:24px"> \
			<h2 class="md-title" style="margin-top:0">Verify Transaction</h2>\
			<p>Please enter the security key to verify this transaction.</p>\
            <md-input-container class="md-icon-float"> \
                <label>Security Key</label> \
				<md-icon md-svg-src="img/ic_vpn_key_24px.svg" class="iconColor" style="color:green;"></md-icon>\
                <input ng-model="securityKey"></input> \
            </md-input-container> \
        </md-content> \
        <div class="md-actions"> \
            <md-button class="md-primary md-button md-default-theme" ng-click="cancelAccept()">Cancel</md-button> \
            <md-button class="md-primary md-button md-default-theme" ng-click="hideAccept()">OK</md-button> \
        </div> \
      </form></md-dialog> \
    ';
	
	$scope.securityKey = '';
	
	//Run this method to bring up accept account window
	$scope.acceptAccount = function(ev,user){
		$mdDialog.show({
                template: acceptContentTemplate,
                targetEvent: ev,
                controller: 'AppCtrl'
            }).then(function (enteredSecurityKey) { //This has two buttons which include accepting a user with a security key and cancelling 
			    //$mdToast.show($mdToast.simple().content('Ok'));
				$mdToast.show($mdToast.simple().content(JSON.stringify(enteredSecurityKey)));
				
				/*
					$pay.bank().confirmAccount($scope.userDetails.TransactionID).success(function(data){
						alert("Successfully Rejected Account");
					}).error(function(data){
						alert("error");
					});
				*/
            }, function() {
                $mdToast.show($mdToast.simple().content('cancelled'));
		});
	}													 
	
	//The two buttons click events below are defined in the acceptContentTemplate
	$scope.hideAccept = function() {
		$mdDialog.hide($scope.securityKey);
	};
	
	$scope.cancelAccept = function() {
		$mdDialog.cancel();
	};
	
	
	//Reject Button
	 var rejectContentTemplate = ' \
      <md-dialog><form> \
        <md-content style="padding:24px"> \
			<h2 class="md-title" style="margin-top:0">Reject Transaction</h2>\
			<p>Please enter the security key to reject this transaction.</p>\
            <md-input-container class="md-icon-float" class="md-icon-float"> \
                <label>Security Key</label> \
				<md-icon md-svg-src="img/ic_vpn_key_24px.svg" class="iconColor" style="color:red;"></md-icon>\
                <input ng-model="securityKey"></input> \
            </md-input-container> \
        </md-content> \
        <div class="md-actions"> \
            <md-button class="md-primary md-button md-default-theme" ng-click="cancelReject()">Cancel</md-button> \
            <md-button class="md-primary md-button md-default-theme" ng-click="hideReject()">OK</md-button> \
        </div> \
      </form></md-dialog> \
    ';

	$scope.rejectAccount = function(ev,user){
		$mdDialog.show({
                template: rejectContentTemplate,
                targetEvent: ev,
                controller: 'AppCtrl'
        }).then(function (enteredsecurityKey) { //This has two buttons which include accepting a user with a security key and cancelling 
			    //$mdToast.show($mdToast.simple().content('Ok'));
				$mdToast.show($mdToast.simple().content(JSON.stringify(user)));
				
				/*
					$pay.bank().confirmAccount($scope.userDetails.TransactionID).success(function(data){
						alert("Successfully Rejected Account");
					}).error(function(data){
						alert("error");
					});
				*/
        }, function() {
                $mdToast.show($mdToast.simple().content('cancelled'));
		});
	}	
	
	//The two buttons click events below are defined in the rejectContentTemplate
	$scope.hideReject = function() {
		$mdDialog.hide($scope.securityKey);
	};
	
	$scope.cancelReject = function() {
		$mdDialog.cancel();
	};
	
		
		 
	$scope.transactions = [
		 	{transactionID:"123456789",
			Date:"21/08/2015",
			cusdecNo:"232323",
			cusdecInfo:"ceylon electricity",
			AmountToPay:"550",
			Company:"dw.duoweb.info",
			AccountNo:"12344321",
			Bank:"Commercial Bank",
			accountHolder:"Prasad Jay",
			Status:"Pending"
		},
		{transactionID:"234567891",
			Date:"28/07/2015",
			cusdecNo:"232323",
			cusdecInfo:"ceylon Water supply",
			AmountToPay:"2550",
			Company:"dw.duoweb.info",
			AccountNo:"12344574",
			Bank:"Sampath Bank",
			accountHolder:"Sajeetharan",
			Status:"Pending"
		},
		{transactionID:"345678912",
			Date:"01/07/2015",
			cusdecNo:"232323",
			cusdecInfo:"ceylon electricity",
			AmountToPay:"1550",
			Company:"dw.duoweb.info",
			AccountNo:"12344321",
			Bank:"HSBC",
			accountHolder:"Dilshan Liyanage",
			Status:"Confirmed"
		},
		{transactionID:"456789123",
			Date:"21/08/2015",
			cusdecNo:"232323",
			cusdecInfo:"CMC",
			AmountToPay:"950",
			Company:"dw.duoweb.info",
			AccountNo:"12367221",
			Bank:"DFCC",
			accountHolder:"Senal Kumarage",
			Status:"Confirmed"
		},
		{transactionID:"567891234",
			Date:"21/08/2015",
			cusdecNo:"232323",
			cusdecInfo:"ceylon electricity",
			AmountToPay:"550",
			Company:"dw.duoweb.info",
			AccountNo:"12344321",
			Bank:"Peoples Bank",
			accountHolder:"Shehan Tis",
			Status:"Rejected"
		},
		{transactionID:"678912345",
			Date:"21/08/2015",
			cusdecNo:"232323",
			cusdecInfo:"ceylon electricity",
			AmountToPay:"550",
			Company:"dw.duoweb.info",
			AccountNo:"12344321",
			Bank:"Commercial Bank",
			accountHolder:"Eranga Manoj",
			Status:"Rejected"
		}
		 ];
	/*
		function loadData(){
			
			var client = $objectstore.getClient("creditNote");
			client.onGetMany(function(data){
				if (data){
					
					for (i = 0, len = $scope.creditNote.length; i<len; ++i){
						if($scope.creditNote.Status == "Pending")
						{
							data.Status = "blue";
						}else if($scope.creditNote.Status == "Confirmed")
						{
							data.Status = "green";
						}else if($scope.creditNote.Status == "Rejected")
						{
							data.Status = "red";
						}
						
						$scope.creditNote = data;
						
					}					
				}
			});	
			
			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('This is embarracing')
						.content('There was an error retreving the data.')
						.ariaLabel('Alert Dialog Demo')
						.ok('OK')
						.targetEvent(data)
				);
			});
			//client.getByFiltering("*", {skip:skilVal, take:takeVal});
			client.getByFiltering("*");	
		}
		loadData();
	*/
	//Front-End Status Filter
	$scope.statusIncludes = [];
		
	$scope.includeStatus = function(Status) {
		var i = $.inArray(Status, $scope.statusIncludes);
		if (i > -1) {
			$scope.statusIncludes.splice(i, 1);
			
		} else {
			$scope.statusIncludes.push(Status);
			
		}
		
	}
	
	$scope.statusFilter = function(userDetails) {
		if ($scope.statusIncludes.length > 0) {
			if ($.inArray(userDetails.Status, $scope.statusIncludes) < 0)
			
				return;
		}
		
		return userDetails;
	}
		

   })//END OF AppCtrl
   
    
	