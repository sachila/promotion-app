angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel','paymentGateway'])

    .config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
	})


   .controller('AppCtrl', function ($scope, $objectstore, $mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $timeout, $q, $http,$pay) {
	
	//parallax scroll effect
	function parallax(){
		 var prlx_lyr_1 = document.getElementById('prlx_lyr_1');
		 var backgound_banner = document.getElementById('backgound_banner');
		 prlx_lyr_1.style.top = -(window.pageYOffset / 10)+'px';
		 backgound_banner.style.top = -(window.pageYOffset / 25)+'px';
	}
	window.addEventListener("scroll", parallax, false);
	
	$scope.recivedTennantCollection;
	//$auth.checkSession();

	//$scope.frameworkShellSecurityToken = $auth.getSecurityToken();
		//console.log($auth.getSecurityToken());
		$auth.checkSession();
	
		function loadProfile()
		{
		   var data=$auth.getSession();

		   var client = $objectstore.getClient("duosoftware.com","profile",true);
		  
		   client.onGetOne(function(data){
				 if (data)         
					$scope.user = data;    
					console.log(data);
			});
				  
			client.onError(function(data){
				   alert ("Error occured");
				   /*createDummyProfile();*/
			});
				  
			   client.getByKey($auth.getUserName());
		}
		
		loadProfile();
		
		$scope.account = "";
		$scope.account.TenantID = "";
		$scope.CardOrAccount = "Account";
		
		$scope.submit = function()
		{
			//console.log(self.selectedItem.display);
			$scope.account.TenantID = self.selectedItem.value;
			$scope.account.CardOrAccount = $scope.CardOrAccount;
			$scope.account.Email = "lasitha44@duosoftware.com"; //This should be from the shell
			
			$pay.account().add($scope.account).success(function(data){

				$mdDialog.show(
				  $mdDialog.alert()
					.parent(angular.element(document.body))
					.title('Request Sent')
					.content('Your request has been submitted. Please wait for the banks confirmation to use this account.')
					.ariaLabel('Alert Dialog Demo')
					.ok('Got it!')
				);
				
			}).error(function(data){
				alert("Error occurred");
			});
			
		}
		
		//Autocomplete stuff
		
		var self = this;
		// list of `state` value/display objects
		self.tenants        = loadAll();
		self.selectedItem  = null;
		self.searchText    = null;
		self.querySearch   = querySearch;
		// ******************************
		// Internal methods
		// ******************************
		/**
		 * Search for tenants... use $timeout to simulate
		 * remote dataservice call.
		 */

		function querySearch (query) {
		
			$scope.enter = function(keyEvent) {
				if (keyEvent.which === 13)
				{	
					if(self.selectedItem === null)
					{
						self.selectedItem = query;	
						console.log(results);
					}else{
						console.log(self.selectedItem);
					}
				}
			}
		  
			//Custom Filter
			var results=[];
			for (i = 0, len = $scope.allBanks.length; i<len; ++i){
				if($scope.allBanks[i].display.indexOf(query) !=-1)
				{
					results.push($scope.allBanks[i]);
				} 
			}
			return results;
		}

		 // Build `tenants` list of key/value pairs
		 
		 $scope.allBanks = [];
		 
		function loadAll() {
		
			$pay.bank().all().success(function(data){
				for (i = 0, len = data.length; i<len; ++i){
					$scope.allBanks.push ({display: data[i].BankName, value:data[i].TenantID});
				}

				}).error(function(data){
					alert ("Error getting all banks");
				});
		
		}


   })//END OF AppCtrl




  