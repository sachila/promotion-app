angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel','paymentGateway'])

    .config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
	})


   .controller('AppCtrl', function ($scope, $objectstore, $mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $q, $http,$pay, $compile, $timeout) {
		
		$scope.email = "";
		
	//	$auth.checkSession();
	
		
		
		$scope.account = "";
		$scope.account.Bank = "";
		$scope.CardOrAccount = "Account";
		
		$scope.submit = function()
		{
			//console.log(self.selectedItem.display);
			//$scope.account.Bank = self.selectedItem.value;
			$scope.account.CardOrAccount = $scope.CardOrAccount;
		//	$scope.account.Email = $auth.getUserName(); //This should be from the shell
			$scope.submitted = true;
			//$scope.account = "";
			console.log($scope.account);
			
			$timeout(function(){
				$scope.submitted = false; // Make submit button enabled again (ng-disabled)
				$scope.account = ""; // Empty the form
				$scope.editForm.$setUntouched();
				 $scope.editForm.$setPristine();
				
				/*
				$scope.account.CardOrAccountNo.$touched = false;
					var myEl = angular.element( document.querySelector( '#accNo' ) );
					myEl.removeClass('ng-touched');*/
					
			}, 3000);
			
			/*
			$pay.account().add($scope.account).success(function(data){

				$mdDialog.show(
				  $mdDialog.alert()
					.parent(angular.element(document.body))
					.title('Request Sent')
					.content('Your request has been submitted. Please wait for the banks confirmation to use this account.')
					.ariaLabel('Alert Dialog Demo')
					.ok('Got it!')
				);
				
			}).error(function(data){
				alert("Error occurred");
			});
			*/
		}
		
		
		
		//Autocomplete stuff
		
		var self = this;
		// list of `state` value/display objects
		self.tenants        = loadAll();
		self.selectedItem  = null;
		self.searchText    = null;
		self.querySearch   = querySearch;
		// ******************************
		// Internal methods
		// ******************************
		/**
		 * Search for tenants... use $timeout to simulate
		 * remote dataservice call.
		 */

		function querySearch (query) {
		
			$scope.enter = function(keyEvent) {
				if (keyEvent.which === 13)
				{	
					if(self.selectedItem === null)
					{
						self.selectedItem = query;	
						console.log(results);
					}else{
						console.log(self.selectedItem);
					}
				}
			}
		  
			//Custom Filter
			var results=[];
			for (i = 0, len = $scope.allBanks.length; i<len; ++i){
				if($scope.allBanks[i].display.indexOf(query) !=-1)
				{
					results.push($scope.allBanks[i]);
				} 
			}
			return results;
		}

		 // Build `tenants` list of key/value pairs
		 
		 $scope.allBanks = [];
		 
		function loadAll() {
		
			$pay.bank().all().success(function(data){
				for (i = 0, len = data.length; i<len; ++i){
					$scope.allBanks.push ({display: data[i].BankName, value:data[i].TenantID});
				}

				}).error(function(data){
					alert ("Error getting all banks");
				});
		
		}


   })//END OF AppCtrl




  