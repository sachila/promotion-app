angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel','paymentGateway'])

	   
   .controller('AppCtrl', function ($scope, $mdDialog, $mdToast,$pay) {
				
		$scope.headerInfo = [];
		
		function load()
		{
			//Get all accounts for the Header
			$pay.bank().searchAccountHolders("*").success(function(accountData){
			
				for (i = 0, len = accountData.length; i<len; ++i){
						$scope.headerInfo.push (
							{Date: accountData[i].RequestedDate, Email: accountData[i].Email , FirstName: accountData[i].FirstName, LastName: accountData[i].LastName, CardOrAccount: accountData[i].CardOrAccount, CardOrAccountNo: accountData[i].CardOrAccountNo, Status: accountData[i].Status }
						);	
							if($scope.headerInfo[i].Status == "Pending")
							{
								$scope.headerInfo[i].StatusColor = "blue";
							}else if($scope.headerInfo[i].Status == "Confirmed")
							{
								$scope.headerInfo[i].StatusColor = "green";
							}else if($scope.headerInfo[i].Status == "Rejected")
							{
								$scope.headerInfo[i].StatusColor = "red";
							}
							
					}
				
			}).error(function(accountData){
				alert ("Error getting all accounts");
			});
			
		}
		load();
		
		//$scope.showLoading = true;
		$scope.loadAll = function(user)
		{
		
			//setTimeout(function(){ 
			//	$scope.$apply(function(){
				$scope.showLoading = false;	
				
			//	});
				
				console.log($scope.showLoading);
			//}, 2000);
			/*
			$pay.bank().searchAccountHolders("*").success(function(accountData){
			
			}).error(function(accountData){
				alert ("Error getting all accounts");
			});
			*/
		}
		
		
		//This template opens when the bank user clicks the Accept Account Button
		var acceptContentTemplate = ' \
      <md-dialog><form> \
        <md-content style="padding:24px"> \
			<h2 class="md-title" style="margin-top:0">Verify Account</h2>\
			<p>Please enter the verification code to verify this account.</p>\
            <md-input-container class="md-icon-float"> \
                <label>Verification Code</label> \
				<md-icon md-svg-src="img/ic_vpn_key_24px.svg" class="iconColor"></md-icon>\
                <input ng-model="verificationCode"></input> \
            </md-input-container> \
        </md-content> \
        <div class="md-actions"> \
            <md-button class="md-primary md-button md-default-theme" ng-click="cancelAccept()">Cancel</md-button> \
            <md-button class="md-primary md-button md-default-theme" ng-click="hideAccept()">OK</md-button> \
        </div> \
      </form></md-dialog> \
    ';
	
	$scope.verificationCode = '';
	
	//Run this method to bring up accept account window
	$scope.acceptAccount = function(ev,user){
		$mdDialog.show({
                template: acceptContentTemplate,
                targetEvent: ev,
                controller: 'AppCtrl'
            }).then(function (enteredverificationCode) { //This has two buttons which include accepting a user with a security key and cancelling 
				
					$mdToast.show($mdToast.simple().content("Accept request sent"));
					
					$pay.bank().confirmAccount(user.CardOrAccountNo, enteredverificationCode).success(function(data){
						$mdDialog.show(
						  $mdDialog.alert()
							.parent(angular.element(document.body))
							.title('Accepted Confirmation')
							.content('Account Successfully Confirmed.')
							.ariaLabel('Alert Dialog Demo')
							.ok('Got it!')
						);
					}).error(function(data){
						alert("error");
					});
				
            }, function() {
                $mdToast.show($mdToast.simple().content('cancelled'));
		});
	}													 
	
	//The two buttons click events below are defined in the acceptContentTemplate
	$scope.hideAccept = function() {
		$mdDialog.hide($scope.verificationCode);
	};
	
	$scope.cancelAccept = function() {
		$mdDialog.cancel();
	};
	
	
	//Reject Button
	 var rejectContentTemplate = ' \
      <md-dialog><form> \
        <md-content style="padding:24px"> \
			<h2 class="md-title" style="margin-top:0">Reject Account</h2>\
			<p>Please enter the verification code to reject this account.</p>\
            <md-input-container class="md-icon-float"> \
                <label>Verification Code</label> \
				<md-icon md-svg-src="img/ic_vpn_key_24px.svg" class="iconColor";"></md-icon>\
                <input ng-model="verificationCode"></input> \
            </md-input-container> \
        </md-content> \
        <div class="md-actions"> \
            <md-button class="md-primary md-button md-default-theme" ng-click="cancelReject()">Cancel</md-button> \
            <md-button class="md-primary md-button md-default-theme" ng-click="hideReject()">OK</md-button> \
        </div> \
      </form></md-dialog> \
    ';

	$scope.rejectAccount = function(ev,user){
		$mdDialog.show({
                template: rejectContentTemplate,
                targetEvent: ev,
                controller: 'AppCtrl'
        }).then(function (enteredverificationCode) { //This has two buttons which include accepting a user with a security key and cancelling 

				$mdToast.show($mdToast.simple().content("Reject request sent"));
				//JSON.stringify(user))
				
					$pay.bank().confirmAccount(user.CardOrAccountNo, enteredverificationCode).success(function(data){
						$mdDialog.show(
						  $mdDialog.alert()
							.parent(angular.element(document.body))
							.title('Accepted Confirmation')
							.content('Account Successfully Rejected.')
							.ariaLabel('Alert Dialog Demo')
							.ok('Got it!')
						);
					}).error(function(data){
						alert("error");
					});
				
        }, function() {
                $mdToast.show($mdToast.simple().content('cancelled'));
		});
	}	
	
	//The two buttons click events below are defined in the rejectContentTemplate
	$scope.hideReject = function() {
		$mdDialog.hide($scope.verificationCode);
	};
	
	$scope.cancelReject = function() {
		$mdDialog.cancel();
	};
	
	
	
	$scope.initial = [
				{FirstName:"Dilshan", LastName:"Liyanage", Status:"Pending", Date:"June 10, 2015",CardOrAccount:"Card", StatusColor:"#303F9F", TransactionID:"45678"},
				{FirstName:"Supun", LastName:"Dissanayaka", Status:"Confirmed", Date:"July 14, 2015",CardOrAccount:"Card",StatusColor:"#4CAF50", TransactionID:"45678"},
				{FirstName:"Eshwaran", LastName:"Veerabahu", Status:"Rejected",Date:"July 14, 2015",CardOrAccount:"Account",StatusColor:"#D32F2F", TransactionID:"45678"},
				{FirstName:"Dilshan", LastName:"Liyanage", Status:"Pending", Date:"June 10, 2015",CardOrAccount:"Card", StatusColor:"#303F9F", TransactionID:"45678"},
				{FirstName:"Supun", LastName:"Dissanayaka", Status:"Confirmed", Date:"July 14, 2015",CardOrAccount:"Card",StatusColor:"#4CAF50", TransactionID:"45678"},
				{FirstName:"Eshwaran", LastName:"Veerabahu", Status:"Rejected",Date:"July 14, 2015",CardOrAccount:"Account",StatusColor:"#D32F2F", TransactionID:"45678"},
				{FirstName:"Dilshan", LastName:"Liyanage", Status:"Pending", Date:"June 10, 2015",CardOrAccount:"Card", StatusColor:"#303F9F", TransactionID:"45678"},
				{FirstName:"Supun", LastName:"Dissanayaka", Status:"Confirmed", Date:"July 14, 2015",CardOrAccount:"Card",StatusColor:"#4CAF50", TransactionID:"45678"},
				{FirstName:"Eshwaran", LastName:"Veerabahu", Status:"Rejected",Date:"July 14, 2015",CardOrAccount:"Account",StatusColor:"#D32F2F", TransactionID:"45678"},
				{FirstName:"Dilshan", LastName:"Liyanage", Status:"Pending", Date:"June 10, 2015",CardOrAccount:"Card", StatusColor:"#303F9F", TransactionID:"45678"},
				{FirstName:"Supun", LastName:"Dissanayaka", Status:"Confirmed", Date:"July 14, 2015",CardOrAccount:"Card",StatusColor:"#4CAF50", TransactionID:"45678"},
				{FirstName:"Eshwaran", LastName:"Veerabahu", Status:"Rejected",Date:"July 14, 2015",CardOrAccount:"Account",StatusColor:"#D32F2F", TransactionID:"45678"},
				{FirstName:"Dilshan", LastName:"Liyanage", Status:"Pending", Date:"June 10, 2015",CardOrAccount:"Card", StatusColor:"#303F9F", TransactionID:"45678"},
				{FirstName:"Supun", LastName:"Dissanayaka", Status:"Confirmed", Date:"July 14, 2015",CardOrAccount:"Card",StatusColor:"#4CAF50", TransactionID:"45678"},
				{FirstName:"Eshwaran", LastName:"Veerabahu", Status:"Rejected",Date:"July 14, 2015",CardOrAccount:"Account",StatusColor:"#D32F2F", TransactionID:"45678"},
				{FirstName:"Dilshan", LastName:"Liyanage", Status:"Pending", Date:"June 10, 2015",CardOrAccount:"Card", StatusColor:"#303F9F", TransactionID:"45678"},
				{FirstName:"Supun", LastName:"Dissanayaka", Status:"Confirmed", Date:"July 14, 2015",CardOrAccount:"Card",StatusColor:"#4CAF50", TransactionID:"45678"},
				{FirstName:"Eshwaran", LastName:"Veerabahu", Status:"Rejected",Date:"July 14, 2015",CardOrAccount:"Account",StatusColor:"#D32F2F", TransactionID:"45678"}

			  ];
		
				
	$scope.userDetails = [
					{FirstName:"Dilshan", LastName:"Liyanage", Mobile:"770902", Email:"duo@abc.com", Country:"Sri Lanka", Status:"Pending", Nic:"3409304343v",Date:"June 10, 2015",CardOrAccount:"Card",CardOrAccountNo:"345678",Expiry: "June 6, 2015",StatusColor:"#303F9F", TransactionID:"45678"},
				    {FirstName:"Supun", LastName:"Dissanayaka", Mobile:"28736823", Email:"dfdf@abc.com", Country:"Sri Lanka", Status:"Confirmed", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Card",CardOrAccountNo:"23453",Expiry: "June 7, 2015",StatusColor:"#4CAF50", TransactionID:"45678"},
					{FirstName:"Eshwaran", LastName:"Veerabahu", Mobile:"34343", Email:"ddfddf@absdfc.com", Country:"India", Status:"Rejected", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Account",CardOrAccountNo:"09757853",StatusColor:"#D32F2F", TransactionID:"45678"},
					{FirstName:"Dilshan", LastName:"Liyanage", Mobile:"770902", Email:"duo@abc.com", Country:"Sri Lanka", Status:"Pending", Nic:"3409304343v",Date:"June 10, 2015",CardOrAccount:"Card",CardOrAccountNo:"7456789",Expiry: "June 6, 2015",StatusColor:"#303F9F", TransactionID:"45678"},
				    {FirstName:"Supun", LastName:"Dissanayaka", Mobile:"28736823", Email:"dfdf@abc.com", Country:"Sri Lanka", Status:"Confirmed", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Card",CardOrAccountNo:"97655678",Expiry: "June 6, 2015",StatusColor:"#4CAF50", TransactionID:"45678"},
					{FirstName:"Eshwaran", LastName:"Veerabahu", Mobile:"34343", Email:"ddfddf@absdfc.com", Country:"India", Status:"Rejected", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Account",CardOrAccountNo:"345678",StatusColor:"#D32F2F"},
					{FirstName:"Dilshan", LastName:"Liyanage", Mobile:"770902", Email:"duo@abc.com", Country:"Sri Lanka", Status:"Pending", Nic:"3409304343v",Date:"June 10, 2015",CardOrAccount:"Card",CardOrAccountNo:"9765567",Expiry: "June 5, 2015",StatusColor:"#303F9F", TransactionID:"45678"},
				    {FirstName:"Supun", LastName:"Dissanayaka", Mobile:"28736823", Email:"dfdf@abc.com", Country:"Sri Lanka", Status:"Confirmed", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Account",CardOrAccountNo:"234454",StatusColor:"#4CAF50"},
					{FirstName:"Eshwaran", LastName:"Veerabahu", Mobile:"34343", Email:"ddfddf@absdfc.com", Country:"India", Status:"Rejected", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Card",CardOrAccountNo:"07654567",Expiry: "June 22, 2015",StatusColor:"#D32F2F", TransactionID:"45678"},
					{FirstName:"Dilshan", LastName:"Liyanage", Mobile:"770902", Email:"duo@abc.com", Country:"Sri Lanka", Status:"Pending", Nic:"3409304343v",Date:"June 10, 2015",CardOrAccount:"Card",CardOrAccountNo:"345678",Expiry: "June 6, 2015",StatusColor:"#303F9F", TransactionID:"45678"},
				    {FirstName:"Supun", LastName:"Dissanayaka", Mobile:"28736823", Email:"dfdf@abc.com", Country:"Sri Lanka", Status:"Confirmed", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Card",CardOrAccountNo:"23453",Expiry: "June 7, 2015",StatusColor:"#4CAF50", TransactionID:"45678"},
					{FirstName:"Eshwaran", LastName:"Veerabahu", Mobile:"34343", Email:"ddfddf@absdfc.com", Country:"India", Status:"Rejected", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Account",CardOrAccountNo:"09757853",StatusColor:"#D32F2F", TransactionID:"45678"},
					{FirstName:"Dilshan", LastName:"Liyanage", Mobile:"770902", Email:"duo@abc.com", Country:"Sri Lanka", Status:"Pending", Nic:"3409304343v",Date:"June 10, 2015",CardOrAccount:"Card",CardOrAccountNo:"7456789",Expiry: "June 6, 2015",StatusColor:"#303F9F", TransactionID:"45678"},
				    {FirstName:"Supun", LastName:"Dissanayaka", Mobile:"28736823", Email:"dfdf@abc.com", Country:"Sri Lanka", Status:"Confirmed", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Card",CardOrAccountNo:"97655678",Expiry: "June 6, 2015",StatusColor:"#4CAF50", TransactionID:"45678"},
					{FirstName:"Eshwaran", LastName:"Veerabahu", Mobile:"34343", Email:"ddfddf@absdfc.com", Country:"India", Status:"Rejected", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Account",CardOrAccountNo:"345678",StatusColor:"#D32F2F", TransactionID:"45678"},
					{FirstName:"Dilshan", LastName:"Liyanage", Mobile:"770902", Email:"duo@abc.com", Country:"Sri Lanka", Status:"Pending", Nic:"3409304343v",Date:"June 10, 2015",CardOrAccount:"Card",CardOrAccountNo:"9765567",Expiry: "June 5, 2015",StatusColor:"#303F9F", TransactionID:"45678"},
				    {FirstName:"Supun", LastName:"Dissanayaka", Mobile:"28736823", Email:"dfdf@abc.com", Country:"Sri Lanka", Status:"Confirmed", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Account",CardOrAccountNo:"234454",StatusColor:"#4CAF50", TransactionID:"45678"},
					{FirstName:"Eshwaran", LastName:"Veerabahu", Mobile:"34343", Email:"ddfddf@absdfc.com", Country:"India", Status:"Rejected", Nic:"9863943v",Date:"July 14, 2015",CardOrAccount:"Card",CardOrAccountNo:"07654567",Expiry: "June 22, 2015",StatusColor:"#D32F2F", TransactionID:"45678"}
				  ];

	
	//Front-End Type Filter
	$scope.typeIncludes = [];
	
	$scope.includeType = function(CardOrAccount) {
		var i = $.inArray(CardOrAccount, $scope.typeIncludes);
		if (i > -1) {
			$scope.typeIncludes.splice(i, 1);
		} else {
			$scope.typeIncludes.push(CardOrAccount);
		}
	}
	
	$scope.typeFilter = function(typeDetails) {
		if ($scope.typeIncludes.length > 0) {
			if ($.inArray(typeDetails.CardOrAccount, $scope.typeIncludes) < 0)
				return;
		}
		return typeDetails;
	}
		
		
	//Front-End Status Filter
	$scope.statusIncludes = [];
		
	$scope.includeStatus = function(Status) {
		var i = $.inArray(Status, $scope.statusIncludes);
		if (i > -1) {
			$scope.statusIncludes.splice(i, 1);
			
		} else {
			$scope.statusIncludes.push(Status);
			
		}
		
	}
	
	$scope.statusFilter = function(userDetails) {
		if ($scope.statusIncludes.length > 0) {
			if ($.inArray(userDetails.Status, $scope.statusIncludes) < 0)
			
				return;
		}
		
		return userDetails;
	}
		

		
})//END OF AppCtrl
   
    