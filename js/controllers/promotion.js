	angular.module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel', 'paymentGateway','ngRoute','ngDragDrop'])
	.config(function($mdThemingProvider) {

		$mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
	})

	.filter('unique', function() {
		return function(collection, keyname) {
			var output = [], 
			keys = [];

			angular.forEach(collection, function(item) {
				var key = item[keyname];
				if(keys.indexOf(key) === -1) {
					keys.push(key);
					output.push(item);
				}
			});
			return output;
		};
	})



	.controller('AppCtrl', function ($scope, $objectstore,$filter,$mdDialog, mfbDefaultValues, $window, $objectstore, $auth, $timeout, $q, $http, $pay) {




		$scope.list1 = [];
		$scope.list2 = [];
		$scope.hiderow = false;

		$scope.selectbox = "1";

		$scope.checkAbility = true;
		$scope.dragdrop = false;
		$scope.products = [];
		$scope.arraychecks = [];
		$scope.productarray = [];
		$scope.promotionarray = [];
		$scope.productdisabled = false;
		$scope.packagedisabled = true;
		$scope.viewproductdisabled = false;
		$scope.viewpackagedisabled = true;


		$scope.droptest2 = function(){


				   sortArrOfObjectsByParam($scope.productitems, "ProductCode");
                   $scope.productitems = removeduplicate($scope.productitems);


	if ($scope.viewselectbox== "1") {

				for (var i = 0; i < $scope.productitems.length; i++) {



						 			for (var j =0; j < $scope.arraychecks.length; j++) {


						 				if ($scope.productitems[i] == $scope.arraychecks[j]) {


											// $scope.arraychecks = angular.copy($scope.products[j]);

											$scope.arraychecks.splice(j, 1);

											console.log("product     "+$scope.arraychecks);


										};

									};

								};


			}

			else if($scope.viewselectbox == "2"){



					for (var i = 0; i < $scope.productitems.length; i++) {



						 			for (var j =0; j < $scope.packagearraycheck.length; j++) {


						 				if ($scope.productitems[i].ProductCode == $scope.packagearraycheck[j].ProductCode) {


											// $scope.arraychecks = angular.copy($scope.products[j]);

											$scope.packagearraycheck.splice(j, 1);

											console.log("promotion     "+$scope.packagearraycheck);


										};

									};

								};

			}





		}


		$scope.test = function(){
			//alert(JSON.stringify($scope.productarray) + "  " + JSON.stringify($scope.list1));

 

			if ($scope.selectbox == "1") {

				for (var i = 0; i < $scope.list1.length; i++) {



						 			for (var j =0; j < $scope.productarray.length; j++) {


						 				if ($scope.list1[i] == $scope.productarray[j]) {


											// $scope.arraychecks = angular.copy($scope.products[j]);

											$scope.productarray.splice(j, 1);

											console.log("product     "+$scope.productarray);


										};

									};

								};


			}

			else if($scope.selectbox == "2"){



					for (var i = 0; i < $scope.list1.length; i++) {



						 			for (var j =0; j < $scope.promotionarray.length; j++) {


						 				if ($scope.list1[i].ProductCode == $scope.promotionarray[j].ProductCode) {


											// $scope.arraychecks = angular.copy($scope.products[j]);

											$scope.promotionarray.splice(j, 1);

											console.log("promotion     "+$scope.promotionarray);


										};

									};

								};

			}

					



		};

		 

		$scope.onselect = function(type){


			console.log(type);


			if (type == "1") {


				if ($scope.selectbox == "2") {
					console.log($scope.selectbox);

					$scope.selectbox = "1";
					$scope.productdisabled = false;
					$scope.packagedisabled = true;



					if ($scope.list1.length > 0) {


						for (var i = $scope.list1.length -1;  i >= 0; i--) {

							$scope.promotionarray.push($scope.list1[i]);
							//$scope.list1.splice($scope.list1[i]);



						};

							$scope.list1 = [];
					};

					console.log($scope.list1);
					console.log($scope.promotionarray);


				}

			}else if(type == "2"){


				if ($scope.selectbox == "1") {

					$scope.selectbox = "2";
					$scope.productdisabled = true;
					$scope.packagedisabled = false;

					if ($scope.list1.length > 0) {
						for (var i = $scope.list1.length -1;  i >= 0; i--) {


							$scope.productarray.push($scope.list1[i]);

									//console.log($scope.productarray);


						};
							$scope.list1 = [];

					};

					console.log($scope.list1);
					console.log($scope.productarray);



				}


			}
		}

		$scope.onviewselect = function(type){


			if (type == "1") {

				$scope.viewproductdisabled = false;
				$scope.viewpackagedisabled = true;


			}else if(type == "2"){

				$scope.viewproductdisabled = true;
				$scope.viewpackagedisabled = false;


			}

		}


		$scope.onviewselectchange = function(type){


			if (type == "1") {

				$scope.viewproductdisabled = false;
				$scope.viewpackagedisabled = true;
				$scope.productitems = [];


			}else if(type == "2"){

				$scope.viewproductdisabled = true;
				$scope.viewpackagedisabled = false;
				$scope.productitems = [];


			}

		}







		$scope.filterIt = function() {

			console.log("filterIt");


			return $filter('orderBy')($scope.productarray,'ProductCode');
		};

			$scope.filterIt2 = function() {

			console.log("filterIt2");


			return $filter('orderBy')($scope.list1,'ProductCode');
		};


		// $scope.filterMe = function(list) {
		// 	return $filter('orderBy')(list, 'ProductCode');
		// };





		$scope.loadAllproduct=function(){
			var client = $objectstore.getClient("dw.duoweb.info","product");
			client.onGetMany(function(data){
				if (data){

									//alert(JSON.stringify(data));
									$scope.products =data;

									console.log($scope.products);


									for (var i = 0; i <= $scope.products.length -1; i++) {
										

										if ($scope.products[i].ProductClass === "Product") {

											$scope.productarray.push($scope.products[i]);

										}

										else if ($scope.products[i].ProductClass === "Package") {

											$scope.promotionarray.push($scope.products[i]);

										};



									};





									console.log($scope.productarray);
									console.log($scope.promotionarray);
									$scope.arraychecks = angular.copy($scope.products);

												// arrayca
							// console.log("array check");

							// console.log($scope.products);
							// console.log($scope.arraychecks);

									// setTimeout(function(){
									// 	$('.collapse-card').paperCollapse();
									// },2000);


			return $scope.products;


			
		}
	});	
			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.title('This is embarracing')
					.content('There was an error retreving the Products.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
					);
			});

			client.getByFiltering("*");







		};
		$scope.viewpromotion = function(){

			$('#view').animate({width:"100%",height:"100%", borderRadius:"0px", right:"0px", bottom:"0px", opacity: 0.25},400, function() { 
				window.location = "Transaction Details.html";
			});

		
		}

		$scope.addpromotion = function(){

				$('#add').animate({width:"100%",height:"100%", borderRadius:"0px", right:"0px", bottom:"0px", opacity: 0.25},400, function() { 
					window.location = "promotion.html";
			});


		
		}

		$scope.savebtn = function(){


			$('#save').animate({width:"100%",height:"100%", borderRadius:"0px", right:"0px", bottom:"0px", opacity: 0.25},400, function() { 
					$('#mySignup').click();
			});
	 

	
		}


		$scope.demo = {
			topDirections: ['left', 'up'],
			bottomDirections: ['down', 'right'],
			isOpen: false,
			availableModes: ['md-fling', 'md-scale'],
			selectedMode: 'md-fling',
			availableDirections: ['up', 'down', 'left', 'right'],
			selectedDirection: 'up'
		};




			// $scope.myFilter = function(){


			// 	return $filter('orderBy')($scope.products, $scope.searchinput);

			// }





			$scope.productitems2 = [];
			


			$scope.arraycheck = function(promotion){

				$scope.productitems2 = angular.copy(promotion);
						 	// $scope.arraychecks = angular.copy($scope.products);
						 	$scope.arraychecks =[];
						 	$scope.packagearraycheck = [];


						 	for (var i = 0; i <= $scope.products.length -1; i++) {


						 		if ($scope.products[i].ProductClass === "Product") {

						 			$scope.arraychecks.push($scope.products[i]);

						 		}

						 		else if ($scope.products[i].ProductClass === "Package") {

						 			$scope.packagearraycheck.push($scope.products[i]);

						 		};



						 	};




						 	console.log("array check");

						 	console.log($scope.arraychecks);
						 	console.log($scope.productitems2);
						 	console.log($scope.packagearraycheck);


						 	if ($scope.productitems.length > 0) {

						 		for (var i = 0; i < $scope.productitems2.length; i++) {



						 			for (var j =0; j < $scope.arraychecks.length; j++) {


						 				if ($scope.productitems2[i].ProductCode == $scope.arraychecks[j].ProductCode) {


											// $scope.arraychecks = angular.copy($scope.products[j]);

											$scope.arraychecks.splice(j, 1);

											console.log("sachila     "+$scope.arraychecks);


										};

									};

								};



								for (var i = 0; i < $scope.productitems2.length; i++) {



									for (var j =0; j < $scope.packagearraycheck.length; j++) {


										if ($scope.productitems2[i].ProductCode == $scope.packagearraycheck[j].ProductCode) {


											// $scope.arraychecks = angular.copy($scope.products[j]);

											$scope.packagearraycheck.splice(j, 1);

											console.log("packagearraycheck     "+$scope.packagearraycheck);


										};

									};

								};




								console.log($scope.arraychecks);
							}

							else{
								console.log("productitems is empty");
							}





						}






						$scope.productdetails = function(ev,index) {


							console.log("working");

							$mdDialog.show({
								controller: DialogController,
								templateUrl: 'partials/product_dialog1.html',
								parent: angular.element(document.body),
								targetEvent: ev,
								locals : {
									item : index
								}
							})
							.then(function(answer) {


								if(answer == "close"){
									$mdDialog.hide();
								}else if(answer == "reset"){

									console.log("reset function");
									$scope.promotion = index;

								}


							}, function() {

							});


						};






						function DialogController($scope, $mdDialog, item) {

							$scope.promotion = item;

							$scope.dialogdisable = true;
							$scope.hide = function() {
								$mdDialog.hide();
							};
							$scope.cancel = function() {
								$mdDialog.cancel();
							};
							$scope.answer = function(answer) {
								$mdDialog.hide(answer);
							};

							$scope.reset = function(){

								console.log("reset function");

								$scope.promotion = item;

							};






							// arraycheck($scope);


						}







						$scope.onChange = function(cbState) {
							if(cbState == true)
							{
								$scope.checkAbility = false;
								$scope.dragdrop = true;

							}else
							{
								$scope.checkAbility = true;
								$scope.dragdrop = false;

							}
						};



	// 					$scope.onDrop = function(ev,ui){
	// 						console.log("ssssssssssssssssssssssssssssssssss");
	// 				// $scope.dropItems.push({
	// 				// 	ProductCode: $scope.promotion.ProductCode

	// 				// });



	// }


	$scope.removeItem = function(index,item){



		    // Appending dialog to document.body to cover sidenav in docs app
		    var confirm = $mdDialog.confirm()
		    .parent(angular.element(document.body))
		    .title('Are You Sure You Want To Delete This Product?')
		    .content()
		    .ariaLabel()
		    .ok('Delete')
		    .cancel('Cancel')
		    .targetEvent(index);

		    $mdDialog.show(confirm).then(function() {



		    	$scope.list1.splice(index, 1);

		    	if (item.ProductClass === "Product") {


		    		$scope.productarray.push(item);
		    	}

		    	else if (item.ProductClass === "Package") {


		    		$scope.promotionarray.push(item);
		    	};

		    	console.log(index)


		    	$mdDialog.show(
		    		$mdDialog.alert()
		    		.parent(angular.element(document.body))
		    		.title()
		    		.content('Successfully Deleted')
		    		.ariaLabel('Alert')
		    		.ok('OK')
		    		.targetEvent(index)
		    		);

		    }, function() {
		    	$mdDialog.hide();
		    });



		};


		$scope.removeItemincards = function(index,item,promotion){


			var confirm = $mdDialog.confirm()
			.parent(angular.element(document.body))
			.title('Are You Sure You Want To Delete This Product?')
			.content()
			.ariaLabel()
			.ok('Delete')
			.cancel('Cancel')
			.targetEvent(index);

			$mdDialog.show(confirm).then(function() {

				   sortArrOfObjectsByParam($scope.productitems, "ProductCode");
                   $scope.productitems = removeduplicate($scope.productitems);

		


				// $scope.testarray = {};

				// $scope.testarray = $scope.productitems.length -1;
				
				// 		console.log($scope.testarray);

				// for (var i = $scope.testarray ; i >= 0 ; i--) {
				// 	if (item.ProductCode == $scope.productitems[i].ProductCode) {

				// 		$scope.productitems.splice(index, 1);
				// 		console.log($scope.productitems.length);
				// 		console.log($scope.productitems);

				// 	};
				// };
				// $scope.productitems = angular.copy($scope.testarray);
	$scope.productitems.splice(index, 1);


				if (item.ProductClass === "Product") {


					if ($scope.arraychecks.indexOf(item) == -1) {


						$scope.arraychecks.push(item);
						console.log($scope.arraychecks);
					};

				}

				else if (item.ProductClass === "Package") {


					if ($scope.packagearraycheck.indexOf(item) == -1) {


						$scope.packagearraycheck.push(item);
						console.log($scope.packagearraycheck);
					};
				};





	$mdDialog.show(
		    		$mdDialog.alert()
		    		.parent(angular.element(document.body))
		    		.title()
		    		.content('Successfully Deleted')
		    		.ariaLabel('Alert')
		    		.ok('OK')
		    		.targetEvent(index)
		    		);

				


			}, function() {
				$mdDialog.hide();
			});



		};



		$scope.optionsList1 = {
			accept: function(dragEl) {
				if ($scope.list1.length >= 5) {
					return false;

				} else {
					return true;
				}
			}
		};

		$scope.dropItems=[];






		$scope.promotion={};
		$scope.newItems=[];


		$scope.table = {
			columns: [{


			}]
		};



	 



		$scope.config = {
			autoHideScrollbar: false,
			theme: 'light',
			advanced:{
				updateOnContentResize: true
			},
			setHeight: 200,
			scrollInertia: 0
		};



		$scope.addItem1 = function() {
			$scope.table.columns.push({

				Currency: '',
				ProductCategory: '',
				ProductDescription:'',
				Qty: '',
				prodCode: '',
				productcode: '',
				rate: ''    

			});
		};
		function setroute(route){

			$location.path(route);


		}



		$scope.scrollbarConfig = {

			autoHideScrollbar: false,
			theme: 'minimal-dark',
			axis: 'y',
			advanced: {
				updateOnContentResize: true
			},
			scrollInertia: 300
		}

		$scope.hideProductDetails = function()
		{
			alert("what");
		}








		$scope.productitems = [];
		$scope.dropItemFunction = function(items,index,type){
			//$scope.productitems.push(items);
			$scope.productitems = angular.copy(items);
			console.log($scope.productitems);

			$scope.onviewselect(type);

			$scope.viewselectbox = type;
		}



		$scope.loadAllOrders=function(){
			var client = $objectstore.getClient("dw.duoweb.info","promotion");
			client.onGetMany(function(data){
				if (data){

									//alert(JSON.stringify(data));
									$scope.promotions =data;
									for (i = 0, len = $scope.promotions.length; i<len; ++i){
										console.log($scope.promotions);

									}

									console.log(data.dropItems);
									// setTimeout(function(){
									// 	$('.collapse-card').paperCollapse();
									// },2000);



			// $scope.productitems.push(angular.copy($scope.promotions[0].dropItems[0]));
			// console.log($scope.productitems);





			return $scope.promotions;
		}
	});	
			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
					.parent(angular.element(document.body))
					.title('This is embarracing')
					.content('There was an error retreving the data.')
					.ariaLabel('Alert Dialog Demo')
					.ok('OK')
					.targetEvent(data)
					);
			});

			client.getByFiltering("*");

		};


		 

		function viewInvoices()
		{
					// $window.location="promotion cards.html";	
					setroute('cards');
				}
				function addInvoice()
				{
					//$window.location="promotion.html";

					setroute('promotion');	
				 	//$scope.dropItems=[];
				 }

				 $scope.dropItemArray = [];

				 $scope.dragsave = function(obj){


	 	// $scope.dropItemArray = $scope.promotions.dropItems;

	 	console.log(obj);
	 // console.log($scope.promotions.code);

	};



	$scope.submit = function(){


		var client = $objectstore.getClient("promotion");

		       //  $scope.promotion.table = $scope.table.columns;
		       $scope.promotion.dropItems = $scope.list1;


		       client.onComplete(function(data){ 
		       	$scope.newItems.push($scope.promotion);

		       	$mdDialog.show(
		       		$mdDialog.alert()
		       		.parent(angular.element(document.body))
		         //.title('This is embarracing')
		         .content('Promotion Successfully Saved')
		         .ariaLabel('')
		         .ok('OK')
		         .targetEvent(data)


		         
		         );

		       	// setroute('cards');
		       });

		       client.onError(function(data){
		       	$mdDialog.show(
		       		$mdDialog.alert()
		       		.parent(angular.element(document.body))
		         //.title('This is embarracing')
		         .content('Error Saving Promotion')
		         .ariaLabel('')
		         .ok('OK')
		         .targetEvent(data)
		         );

		       });

		       $scope.promotion.code="1111";
		       client.insert($scope.promotion, {KeyProperty:"code"});
		   };





		   $scope.edit = function(updatedForm)
		   {

		   	var client = $objectstore.getClient("promotion");

		       //  $scope.promotion.table = $scope.table.columns;




		       sortArrOfObjectsByParam($scope.productitems, "ProductCode");

		       console.log($scope.productitems);


	 

		$scope.productitems = removeduplicate($scope.productitems);

		console.log($scope.productitems);

		// var resp = $filter('mySort')($scope.productitems,$scope.productitems.ProductCode);




		updatedForm.dropItems = angular.copy($scope.productitems);

		console.log($scope.promotion.dropItems);

		client.onComplete(function(data){ 
			$scope.newItems.push($scope.promotion);

			$mdDialog.show(
				$mdDialog.alert()
				.parent(angular.element(document.body))
		         //.title('This is embarracing')
		         .content('Promotion Successfully Updated')
		         .ariaLabel('')
		         .ok('OK')
		         .targetEvent(data)


		         
		         );


		});

		client.onError(function(data){
			$mdDialog.show(
				$mdDialog.alert()
				.parent(angular.element(document.body))
		         //.title('This is embarracing')
		         .content('Error Updating Promotion')
		         .ariaLabel('')
		         .ok('OK')
		         .targetEvent(data)
		         );

		});

		            // $scope.creditNote.number="12";
		            client.insert(updatedForm, {KeyProperty:"code"});

		    //  console.log(updatedForm);
		}





		function mainAction() {
					  //console.log('Firing Main Action!');
					}

					function setMainAction() {
						if(vm.chosen.action === 'fire') {
							vm.mainAction = mainAction;
						} else {
							vm.mainAction = null;
						}
					}







					$scope.myAccounts = [
					{DisplayName:"Bank Account A", AccountNo:"ending with 123", Bank:"Bank of Ceylon"},
					{DisplayName:"Bank Account B", AccountNo:"ending with 345", Bank:"HNB"},
					{DisplayName:"Bank Account C", AccountNo:"ending with 456", Bank:"NSB"},
					];



			//Autocomplete stuff
			
			var self = this;
			// list of `state` value/display objects
			self.tenants        = loadAll();
			self.selectedItem  = null;
			self.searchText    = null;
			self.querySearch   = querySearch;
			// ******************************
			// Internal methods
			// ******************************
			/**
			 * Search for tenants... use $timeout to simulate
			 * remote dataservice call.
			 */

			 function querySearch (query) {

			 	$scope.enter = function(keyEvent) {
			 		if (keyEvent.which === 13)
			 		{	
			 			if(self.selectedItem === null)
			 			{
			 				self.selectedItem = query;	
			 				console.log(results);
			 			}else{
			 				console.log(self.selectedItem);
			 			}
			 		}
			 	}

				//Custom Filter
				var results=[];
				for (i = 0, len = $scope.allBanks.length; i<len; ++i){
					if($scope.allBanks[i].display.indexOf(query) !=-1)
					{
						results.push($scope.allBanks[i]);
					} 
				}
				return results;
			}

			 // Build `tenants` list of key/value pairs
			 
			 $scope.allBanks = [];
			 
			 function loadAll() {

	 

			 }



			 

	   })//END OF AppCtrl

	function sortArrOfObjectsByParam(arrToSort /* array */, strObjParamToSortBy /* string */, sortAscending /* bool(optional, defaults to true) */) {
    if(sortAscending == undefined) sortAscending = true;  // default to true
    
    if(sortAscending) {
    	arrToSort.sort(function (a, b) {
    		return a[strObjParamToSortBy] > b[strObjParamToSortBy];
    	});
    }
    else {
    	arrToSort.sort(function (a, b) {
    		return a[strObjParamToSortBy] < b[strObjParamToSortBy];
    	});
    }
}


function removeduplicate(myarr){

	var arr = {};

	for ( var i=0; i < myarr.length; i++ )
		arr[myarr[i]['ProductCode']] = myarr[i];

	var things = new Array();
	for ( var key in arr )
		things.push(arr[key]);

	return things;
}





