angular
    .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel', 'paymentGateway'])

	   
   .controller('AppCtrl', function ($scope, $mdDialog, $pay) {
   
		
	$scope.headerInfo = [];
	

		
		function loadAccountData(callback)
		{
			$pay.account().getUserAccounts().success(function(accountData){
			
				$scope.headerInfo = accountData;
				
				callback();
			}).error(function(data){
				alert("error");
			});
			
		}
		
		loadAccountData(loadStatusData);
		
		$scope.StatusData = [];
		
		function loadStatusData()
		{
			
			for (i = 0, len = $scope.headerInfo.length; i<len; ++i){

				$pay.account().status($scope.headerInfo[i].CardOrAccountNo).success(function(status){
					
					for (i = 0, len = $scope.headerInfo.length; i<len; ++i){
					
						if($scope.headerInfo[i].CardOrAccountNo == status.CardOrAccountNo)
						{
							$scope.headerInfo[i].Status = status.Status;
							if($scope.headerInfo[i].Status == "Pending")
							{
								$scope.headerInfo[i].StatusColor = "blue";
								console.log($scope.headerInfo[i]);
								
							}
							else if($scope.headerInfo[i].Status == "Confirmed")
							{
								$scope.headerInfo[i].Status = "Active";
								$scope.headerInfo[i].StatusColor = "green";
								console.log($scope.headerInfo[i]);
								
							}else if($scope.headerInfo[i].Status == "Rejected")
							{
								$scope.headerInfo[i].Status = "Inactive";
								$scope.headerInfo[i].StatusColor = "red";
								console.log($scope.headerInfo[i]);
							}
							
							
						}
					}
				}).error(function(data){
					alert("error");
				});
			}
			
			
		}
		
		$scope.showCode = function(accoutNo)
		{
			$pay.account().getVerificationCode(accoutNo).success(function(status){
			//	console.log(status.Verification);
				
				$mdDialog.show(
					$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('Verification Code')
						.content('Your account Verification code is '+ status.Verification)
						.ariaLabel('Alert Dialog Demo')
						.ok('OK')
				);
				
			}).error(function(data){
					alert("error");
			});
		}
	
		
		
		$scope.accDetails = [
					{Bank:"HNB", CardOrAccountNo:"ending with 2342", CardOrAccount:"Card",CardOrAccountType:"VISA", NameOnCardOrAccount:"Mr. Perera", Expiry:"August 10, 2015", DisplayName:"My Nice Card", Status:"Pending"},
					{Bank:"NSB", CardOrAccountNo:"ending with 2334",  CardOrAccount:"Card", CardOrAccountType:"VISA", NameOnCardOrAccount:"Mr. Dilshan", Expiry:"August 16, 2015", DisplayName:"Card 2", Status:"Inactive"},
					{Bank:"Bank of Ceylon", CardOrAccountNo:"ending with 2542",  CardOrAccount:"Card", CardOrAccountType:"VISA", NameOnCardOrAccount:"Dobile", Expiry:"November 16, 2015", DisplayName:"Card 4", Status:"Active"},
					{Bank:"Bank of Ceylon", CardOrAccountNo:"ending with 5323",  CardOrAccount:"Card", CardOrAccountType:"VISA", NameOnCardOrAccount:"whart", Expiry:"November 16, 2015", DisplayName:"Card 6", Status:"Active"},
					{Bank:"HSBC", CardOrAccountNo:"ending with 7435",  CardOrAccount:"Card", CardOrAccountType:"VISA", NameOnCardOrAccount:"dlkfd", Expiry:"November 16, 2015", DisplayName:"Card 7", Status:"Inactive"},
					{Bank:"NSBB", CardOrAccountNo:"ending with 2334",  CardOrAccount:"Account", CardOrAccountType:"VISA", NameOnCardOrAccount:"Mr. Dilshan", Expiry:"August 16, 2015", DisplayName:"Card 2", Status:"Inactive"},
					{Bank:"Bank of Ceylonn", CardOrAccountNo:"ending with 2542",  CardOrAccount:"Account", CardOrAccountType:"VISA", NameOnCardOrAccount:"Dobile", Expiry:"November 16, 2015", DisplayName:"Card 4", Status:"Active"}
				  ];

	/*
		function loadData(){
			
			var client = $objectstore.getClient("creditNote");
			client.onGetMany(function(data){
				if (data){
					
					for (i = 0, len = $scope.creditNote.length; i<len; ++i){
						if($scope.creditNote.Status == "Pending")
						{
							data.Status = "blue";
						}else if($scope.creditNote.Status == "Active")
						{
							data.Status = "green";
						}else if($scope.creditNote.Status == "Inactive")
						{
							data.Status = "red";
						}
						
						$scope.creditNote = data;
						
					}					
				}
			});	
			
			client.onError(function(data){
				$mdDialog.show(
					$mdDialog.alert()
						.parent(angular.element(document.body))
						.title('This is embarracing')
						.content('There was an error retreving the data.')
						.ariaLabel('Alert Dialog Demo')
						.ok('OK')
						.targetEvent(data)
				);
			});
			//client.getByFiltering("*", {skip:skilVal, take:takeVal});
			client.getByFiltering("*");	
		}
		loadData();
	*/
	
		//Type Filter
		$scope.typeIncludes = [];
		
		$scope.includeType = function(CardOrAccount) {
			var i = $.inArray(CardOrAccount, $scope.typeIncludes);
			if (i > -1) {
				$scope.typeIncludes.splice(i, 1);
			} else {
				$scope.typeIncludes.push(CardOrAccount);
			}
		}
		
		$scope.typeFilter = function(typeDetails) {
			if ($scope.typeIncludes.length > 0) {
				if ($.inArray(typeDetails.CardOrAccount, $scope.typeIncludes) < 0)
					return;
			}
			return typeDetails;
		}
		
		//Status Filter
		$scope.statusIncludes = [];
			
		$scope.includeStatus = function(Status) {
			var i = $.inArray(Status, $scope.statusIncludes);
			if (i > -1) {
				$scope.statusIncludes.splice(i, 1);
			} else {
				$scope.statusIncludes.push(Status);
			}
		}
		
		$scope.statusFilter = function(userDetails) {
			if ($scope.statusIncludes.length > 0) {
				if ($.inArray(userDetails.Status, $scope.statusIncludes) < 0)
					return;
			}
			
			return userDetails;
		}
		
   })//END OF AppCtrl
   
    
	
	